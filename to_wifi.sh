#!/bin/sh
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ "$(nmcli con show --active | grep hotspot)" ]; then
	nmcli con down hotspot
	nmcli con modify hotspot connection.autoconnect no

	sleep 10
	$DIR/forward_ports_start.sh
fi
