#!/bin/sh
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
install $DIR/network_setup.service -m 664 /etc/systemd/system/

systemctl daemon-reload
systemctl enable network_setup
