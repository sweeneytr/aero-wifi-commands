#!/bin/sh
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ ! "$(nmcli con show --active | grep hotspot)"]; then
	$DIR/forward_ports_stop.sh

	nmcli con up hotspot
	nmcli con modify hotspot connection.autoconnect yes
fi
