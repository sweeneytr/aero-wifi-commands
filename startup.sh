#!/bin/sh
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
WIFI_SSID_PREFIX="NUwave"

$DIR/to_wifi.sh
# sleep 10 # to_wifi already sleeps between con up and port forward

if [ ! "$(nmcli con show --active | grep -i $WIFI_SSID_PREFIX))" ]; then
	echo $DIR/to_hotspot.sh
fi
