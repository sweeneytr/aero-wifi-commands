#!/bin/sh
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
$DIR/forward_ports_stop.sh

ssh -p 2223 -NR 0.0.0.0:2222:localhost:22   aero@calcifer.net \
	1>$DIR/log/ssh.ssh.log 2>&1 & echo $! > $DIR/pids/ssh.pid
ssh -p 2223 -NR 0.0.0.0:8554:localhost:8554 aero@calcifer.net \
	1>$DIR/log/rstp.ssh.log 2>&1 & echo $! > $DIR/pids/rstp.pid
